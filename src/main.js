import { createApp } from "vue";
import App from "./App.vue";
import router from "./router"
// import VueToastr from "vue-toastr";

import { Button, Input, Form, Menu, Layout, Tag, Table,  } from "ant-design-vue";

const app = createApp(App);

app.use(Button);
app.use(Input);
app.use(Form);
app.use(Menu);  
app.use(Layout);
app.use(Tag);
app.use(Table);
app.use(router);
// app.use(VueToastr);



app.mount("#app");
