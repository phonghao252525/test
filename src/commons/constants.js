export default {
  FORMAT_DATE: "DD/MM/YYYY",
  FORMAT_TIME: "hh:mm:ss",
  FORMAT_DATE_TIME: "DD/MM/YYYY hh:mm:ss",
  DEACTIVE: "deactive",
  ACTIVE: "active",
  SMS_STATUS: {
    SUCCESS: "success",
    FAILED: "failed",
  },
};
