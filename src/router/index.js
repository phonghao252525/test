import { createRouter, createWebHistory } from 'vue-router'


import SendSMS from "../pages/SendSMS.vue";
import Overview from "../pages/Overview.vue";
import SMSTemplate from "../pages/SMSTemplate.vue";
import DetailDevice from "../pages/DetailDevice.vue";
import History from "../pages/History.vue";
import ForgotPassword from "../pages/ForgotPassword.vue";
import SignUp from "../pages/SignUp.vue";
import Login from "../pages/Login.vue";
import Home from "../pages/Home.vue";


const routes = [
    { path: '/', component: Login},
    { path: '/overview', component: Overview},

    { path: '/send-sms', component: SendSMS},

    { path: '/content-sms', component: SMSTemplate},
    { path: '/detail-device', component: DetailDevice},
    { path: '/history', component: History},
    { path: '/forgotpassword', component: ForgotPassword},
    { path: '/sign-up', component: SignUp},
    { path: '/home', component: Home},
]

const router = createRouter({
    history: createWebHistory(),
    routes: routes
})

export default router
